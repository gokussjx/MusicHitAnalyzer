from __future__ import print_function

import os.path
import librosa
import librosa.display
import matplotlib.pyplot as plt


# Run the default beat tracker
def find_tempo(filename):
    # Load the audio as a waveform `y`
    # Store the sampling rate as `sr`
    y, sr = librosa.load(filename)
    tempo, beat_frames = librosa.beat.beat_track(y=y, sr=sr)
    print(filename)
    print('Estimated tempo: {:.2f} beats per minute'.format(tempo))

def find_cur_path(components):
    result = ''
    for compo in components:
        result += compo
        result += "/"
    return result

def report(directory):
    for root, dirs, files in os.walk("."):
        for file in files:
            if file.endswith('.mp3'):
                fileLocation = find_cur_path(root.split(os.sep)) + file
                find_tempo(fileLocation)

report("./resources/sampleData/")

# # 4. Convert the frame indices of beat events into timestamps
# beat_times = librosa.frames_to_time(beat_frames, sr=sr)

# #print('Saving output to beat_times.csv')
# #librosa.output.times_csv('beat_times.csv', beat_times)

# y, sr = librosa.load(filename, duration=10)
# plt.figure()
# plt.subplot(3, 1, 1)
# librosa.display.waveplot(y, sr=sr)
# plt.title('Monophonic')

# # Or a stereo waveform

# y, sr = librosa.load(filename,
#                      mono=False, duration=10)
# plt.subplot(3, 1, 2)
# librosa.display.waveplot(y, sr=sr)
# plt.title('Stereo')

# # Or harmonic and percussive components with transparency

# y, sr = librosa.load(filename, duration=10)
# y_harm, y_perc = librosa.effects.hpss(y)
# plt.subplot(3, 1, 3)
# librosa.display.waveplot(y_harm, sr=sr, alpha=0.25)
# librosa.display.waveplot(y_perc, sr=sr, color='r', alpha=0.5)
# plt.title('Harmonic + Percussive')
# plt.tight_layout()
